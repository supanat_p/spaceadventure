var Ship = cc.Sprite.extend({
    ctor: function() {
        this._super();
        this.initWithFile( 'res/images/ship.png' );
    },

    moveUp : function() {
    	var posY = this.getPositionY();
        if ( posY < screenHeight - 10 ) {
            posY += 10;
            this.setPositionY( posY );
        }
    },

    moveDown : function() {
    	var posY = this.getPositionY();
        if ( posY > 10 ) {
            posY -= 10;
            this.setPositionY( posY );
        }
    },

    hit : function(bulletY){
    	return (Math.abs( bulletY - self.ship.getPositionY() ) < 25);
    }
});
