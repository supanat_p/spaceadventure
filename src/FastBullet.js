var FastBullet = Bullet.extend({

    init : function() {
        this.initWithFile( 'res/images/bullet.png' );
    },

    move : function() {
        var posX = this.getPositionX();
        posX += FastBullet.SPEED;
        this.setPositionX( posX );
    }
});

FastBullet.SPEED = 10;