var Bullet = cc.Sprite.extend({

	ctor: function() {
        this._super();
        this.init();
    },

	randomCreate : function() {
		this.randomPosition();
	},

	randomPosition : function() {
        var disY = 10 + Math.floor( Math.random() * ( screenHeight - 10 ) );
        this.setPosition( new cc.Point( 100, disY ) );
    },

    update : function(dt) {
    	this.move();
    	var x = this.getPositionX();
    	if(x>screenWidth)
    		this.randomPosition();
    }
});