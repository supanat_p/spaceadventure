var GameLayer = cc.LayerColor.extend({
    init: function() {
        this._super( new cc.Color( 0, 0, 0, 255 ) );
        this.setPosition( new cc.Point( 0, 0 ) );

        this.createShip();
        this.createBullet();
        this.createLife();
        
        this.addKeyboardHandlers();
        this.scheduleUpdate();
        
        return true;
    },

    createShip : function() {
        this.ship = new Ship();
        this.ship.setPosition( new cc.Point( 700, 300 ) );
        this.addChild( this.ship );
    },

    createBullet : function(){
        this.bullets = [];
        for ( var i = 0; i < 15; i++ ) {
            var num = Math.random();
            var bullet = null; 
            if ( this.isSlowBullet(num)) {
                bullet = new SlowBullet();
            } else if ( this.isFastBullet(num) ) {
                bullet = new FastBullet();
            } else {
                bullet = new WaveBullet();
            }
            bullet.randomCreate();
            bullet.setPositionX( 100 - 150 * i );
            this.addChild( bullet );
            bullet.scheduleUpdate();

            this.bullets.push( bullet );
        }
    },

    isSlowBullet : function(num) {
        return (num<=0.4);
    },

    isFastBullet : function(num) {
        return (num>0.4) && (num<=0.7);
    },

    isWaveBullet : function(num) {
        return (num>0.7);
    },

    createLife : function() {
        this.life = 10;
        this.lifeLabel = cc.LabelTTF.create( 10, 'Arial', 32 );
        this.lifeLabel.setPosition( cc.p( 700, 550 ) );
        this.addChild( this.lifeLabel );
    },

    update: function( dt ) {
        var self = this;
        this.bullets.forEach( function( bullet, i ) {
            var x = bullet.getPositionX();
            if ( ( x < screenWidth ) &&
                 ( x > screenWidth - 100 ) ) {
                var y = bullet.getPositionY();
                if ( self.ship.hit(y) ) {
                    bullet.randomPosition();
                    this.decreaseLife();
                    this.checkEndGame();
                    return;
                }
            }
        });
    },
    
    decreaseLife : function(){
        this.life -= 1;
        this.lifeLabel.setString( this.life );
    },

    checkEndGame : function(){
        if ( this.life == 0 ) {
            var gameOverLabel = cc.LabelTTF.create( 'Game over', 'Arial', 60 );
            gameOverLabel.setPosition( cc.p( 400, 300 ) );
            this.addChild( gameOverLabel );
            cc.director.pause();
        }
    }
    onKeyDown: function( keyCode, event ) {
        if ( keyCode == cc.KEY.up ) {
            this.ship.moveUp();
        } else if ( keyCode == cc.KEY.down ) {
            this.ship.moveDown();
        }
    },
    
    onKeyUp: function( keyCode, event ) {
    },
    
    addKeyboardHandlers: function() {
        var self = this;
        cc.eventManager.addListener({
            event: cc.EventListener.KEYBOARD,
            onKeyPressed : function( keyCode, event ) {
                self.onKeyDown( keyCode, event );
            },
            onKeyReleased: function( keyCode, event ) {
                self.onKeyUp( keyCode, event );
            }
        }, this);
    }
});
 
var StartScene = cc.Scene.extend({
    onEnter: function() {
        this._super();
        var layer = new GameLayer();
        layer.init();
        this.addChild( layer );
    }
});
