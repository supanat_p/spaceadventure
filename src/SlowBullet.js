var SlowBullet = Bullet.extend({

    init : function() {
         this.initWithFile( 'res/images/bullet.png' );
    },

    move : function() {
        var posX = this.getPositionX();
        posX += SlowBullet.SPEED;
        this.setPositionX( posX );
    }
});

SlowBullet.SPEED = 5;
