var WaveBullet = Bullet.extend({

    init : function() {
        this.initWithFile( 'res/images/bullet.png' );
        this.step = 0;
        this.period = 30.0 + Math.random() * 20;
    },

    move : function() {
        var pos = this.getPosition();

        this.step += WaveBullet.SPEED;
        this.setPosition( cc.p( pos.x + 5, pos.y + 4 * Math.sin( Math.PI * this.step / this.period ) ) );
    }

});

WaveBullet.SPEED = 1;
